# Sample project

```mermaid
graph TD
    a --->|r| B
    B -->|g| C
    D -->|u| a
    H -->|p| a

    subgraph "sub"
    B
    subgraph "subsub"
    direction TB
    C -->|d| F
    F --> D & G
    G -->|p| E
    D --> E
    E --> H
    end
    end
```
